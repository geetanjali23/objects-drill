// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values
function values(obj) {
    if (typeof obj != "object" || Array.isArray(obj) || typeof obj === undefined) {
        return [];
    }
    let valuesArr = [];
    for(let key in obj) {
        if(!(typeof obj[key] === "function")) {
            valuesArr.push(obj[key]);
        }
    }
    return valuesArr;
}
module.exports = values;