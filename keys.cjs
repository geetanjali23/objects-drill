// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys
function keys(obj) {
    if (typeof obj != "object" || Array.isArray(obj) || typeof obj === undefined) {
        return [];
    }
    let keysArr = [];
    for (let key in obj) {    
            keysArr.push(key);
    }
    return keysArr;
}
module.exports = keys;

