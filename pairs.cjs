// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs
function pairs(obj) {
    if (typeof obj != "object" || Array.isArray(obj) || typeof obj === undefined) {
        return [];
    }
    let result = [];
    for (let key in obj) {
        if (obj[key] != undefined) {
            result.push([key, obj[key]]);
        }
    }
    return result;
}
module.exports = pairs;