// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults
function defaults(obj, defaultProps) {
    if (typeof obj != "object" || Array.isArray(obj) || typeof obj === undefined) {
        return [];
    }
    for (let key in obj) {
        if (obj[key] === undefined) {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}
module.exports = defaults;