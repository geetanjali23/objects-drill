// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject
function mapObject(obj, cb) {
    if (typeof obj != "object" || Array.isArray(obj) || typeof obj === undefined) {
        return [];
    }
    let result = {};
    for (let key in obj) {
        result[key] = cb(obj[key]);
    }
    return result;
}
module.exports = mapObject;