function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    if (typeof obj != "object" || Array.isArray(obj) || typeof obj === undefined) {
        return [];
    }
    let resObj = {};
    for(let property in obj) {
            resObj[obj[property]] = property;
    }
    return resObj;
}
module.exports = invert;